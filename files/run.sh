#!/bin/sh


mkdir -p /run/nginx
chown nginx:nginx /run/nginx

# Fix permission issues on mounted volume in macOS
sed -i "s/^nobody:.*$/nobody:x:1000:50::nobody:\/:\/sbin\/nologin/" /etc/passwd
sed -i "s/^nobody:.*$/nobody:x:50:/" /etc/group

sed -i "s/^\$dbuser = .*$/\$dbuser = \"${MYSQL_USER}\";/" /var/www/html/ulogger-server/config.php
sed -i "s/^\$dbpass = .*$/\$dbpass = \"${MYSQL_PASSWORD}\";/" /var/www/html/ulogger-server/config.php

sed -i "s/^\$dbdsn = .*$/\$dbdsn = \"mysql:host=ulogger-db;port=3306;dbname=${MYSQL_DATABASE};charset=utf8\";/" /var/www/html/ulogger-server/config.php

# set config variables

if [ "${ULOGGER_ENABLE_SETUP}" = "1" ]; then
  sed -i "s/\$enabled = false;/\$enabled = true;/" /var/www/html/ulogger-server/scripts/setup.php;
  echo "ulogger setup script enabled"
  echo "----------------------------"
fi

# show config variables
echo "ulogger configuration"
echo "---------------------"
grep '^\$' /var/www/html/ulogger-server/config.php

nginx
php-fpm7 -F
