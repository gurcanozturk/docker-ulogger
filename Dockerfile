FROM alpine:latest

ENV ULOGGER_ENABLE_SETUP 0

RUN apk add --no-cache \
    nginx && \
    git && \
    php7-ctype && \ 
    php7-fpm && \
    php7-json && \ 
    php7-pdo && \ 
    php7-session && \ 
    php7-simplexml && \ 
    php7-xmlwriter && \ 
    php7-pdo_mysql && \
    rm -rf /var/www/html && mkdir -p /var/www/html && \
    cd /var/www/html && git clone https://github.com/bfabiszewski/ulogger-server.git && \
    rm -rf /var/cache/apk/*

COPY files/run.sh /run.sh
COPY files/nginx.conf /etc/nginx/conf.d/default.conf
COPY files/config.default.php /var/www/html/ulogger-server/config.default.php

RUN chmod +x /run.sh && \
    chown nginx.nginx /etc/nginx/conf.d/default.conf /var/www/html/ulogger-server && \
    grep '^[$<?]' /var/www/html/ulogger-server/config.default.php > /var/www/html/ulogger-server/config.php

EXPOSE 80

CMD ["/run.sh"]